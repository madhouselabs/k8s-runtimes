IMAGE_REGISTRY = "registry.gitlab.com/madhouselabs/k8s-runtimes"

BUILD_ARGS := ""
TAG :="latest"
DFILE="Dockerfile"
SHELL := $(shell which bash)

.docker: IMAGE_NAME=$(shell echo ${APP} | tr "/" "-" | sed 's/\-/\//')
.docker: .build .push

.build: BUILD_ID=$(shell date -Iseconds)
.build:
	cd $(shell dirname ${DFILE}) && eval docker build -f $(shell basename ${DFILE}) -t $(IMAGE_REGISTRY)/${IMAGE_NAME}:${TAG} . \
		$(BUILD_ARGS)

.push:
	docker push $(IMAGE_REGISTRY)/${IMAGE_NAME}:${TAG}

node.alpine: APP = "node"
node.alpine: TAG="latest"
node.alpine: DFILE = "node/Dockerfile.alpine"
node.alpine: .docker

node.slim: APP = "node"
node.slim: TAG="slim"
node.slim: DFILE = "Dockerfile.slim"
node.slim: .docker

nginx: APP = "nginx"
nginx: .docker

tekton: APP = "tekton"
tekton: DFILE = "./tekton/Dockerfile"
tekton: .docker

node-kafka: APP="node-kafka"
node-kafka: .docker

ubuntu: APP=ubuntu
ubuntu: .docker
ubuntu: BUILD_ARGS="--build-arg rootPassword=hotspot --build-arg userName=app"
ubuntu: TAG="notroot"

kjob: APP=kjob
kjob: .docker

kjob.mongo: TAG=mongo
kjob.mongo: DFILE="Dockerfile.mongo"
kjob.mongo: kjob

kjob.node: TAG=node
kjob.node: DFILE="kjob/Dockerfile.node"
kjob.node: kjob

alpine-git: APP=git
alpine-git: DFILE="./alpine-git/Dockerfile"
alpine-git: .docker
