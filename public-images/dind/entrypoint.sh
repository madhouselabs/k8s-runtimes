#!/bin/sh
for i in $(seq 1 10)
do
  sleep 1
  unset DOCKER_HOST
  if [ -f /var/run/docker.pid ]; then
    cd ${SRC_PKG}

    if [ -f .kloudlite.yml ]; then
      cat .kloudlite.yml | yq -o json .fission | jq -r 'to_entries | map("\(.key)=\"\(.value|tostring)\"") |.[]' > /tmp/vars
      source /tmp/vars
    fi


    echo "############################################"
    echo "BASE_IMAGE: " $BASE_IMAGE
    echo "SRC_PKG: " $SRC_PKG
    echo "DEPLOY_PKG: " $DEPLOY_PKG
    echo "OUTPUT_DIR:" $OUTPUT_DIR
    echo "BUILD: " $BUILD
    echo "############################################"


    if [ -z "$OUTPUT_DIR" ]; then
      docker run --workdir /app  -v $SRC_PKG:/app --rm $BASE_IMAGE sh -c "$BUILD"
      cp -r $SRC_PKG $DEPLOY_PKG
    else
      docker run --workdir /app  -v $SRC_PKG:/app -v $PWD/$OUTPUT_DIR:/app/$OUTPUT_DIR --rm $BASE_IMAGE sh -c "$BUILD"
      cp -r $OUTPUT_DIR $DEPLOY_PKG
    fi
    exit 0
  else
    nohup dockerd &
    sleep 2
  fi
done
