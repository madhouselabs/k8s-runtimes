#!/bin/sh
BASE_IMAGE=alpine
OUTPUT_DIR=/tmp/y

SRC_PKG="/tmp/x"
DEPLOY_PKG="/tmp/y"
BUILD="sh ./hello.sh"

echo "############################################"
echo "SRC_PKG: " $SRC_PKG
echo "DEPLOY_PKG: " $DEPLOY_PKG
echo "OUTPUT_DIR:" $OUTPUT_DIR
echo "BUILD: " $BUILD
echo "############################################"

mkdir /tmp/x
cd ${SRC_PKG}
echo "echo hello world > out" > ./hello.sh
export DOCKER_HOST="tcp://dind.hotspot:2375"
docker run --workdir /app  -v $SRC_PKG:/app -v $DEPLOY_PKG:/$OUTPUT_DIR --rm $BASE_IMAGE sh -c "$BUILD"

