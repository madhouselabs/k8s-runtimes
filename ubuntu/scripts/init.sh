curl "https://git.madhouselabs.io/-/snippets/3/raw/master/.zshrc" > ~/.zshrc

[ -d $HOME/bin ] || mkdir $HOME/bin
echo 'export PATH=$HOME/bin:$PATH' >> $HOME/.zshrc

[ -d $HOMe/.config ] || mkdir $HOME/.config

curl "https://git.madhouselabs.io/-/snippets/3/raw/master/starship.toml" > $HOME/.config/starship.toml

cat > $HOME/.npmrc <<EOF
global-dir=${HOME}/.local/share/node
global-bin-dir=${HOME}/.local/share/node/bin
prefix=${HOME}/.local/share/node
@madhouselabs:registry=https://gitlab.com/api/v4/packages/npm
//gitlab.com/api/v4/packages/npm/:_authToken=${NPM_REGISTRY_TOKEN}
EOF
