npm i -g pnpm

# installing neovim
curl -L https://github.com/neovim/neovim/releases/latest/download/nvim-linux64.tar.gz > nvim.tar.gz 
tar xf nvim.tar.gz 
mv nvim-linux64/bin/nvim $HOME/bin/nvim 
rm -rf nvim.tar.gz nvim-linux64

# installing kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" 
chmod +x ./kubectl
mv ./kubectl ~/bin/kubectl

# installing docker
curl https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz > docker.tgz
tar xf docker.tgz
mv docker/docker ~/bin/docker
rm -rf docker
