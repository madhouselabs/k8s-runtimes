RSA_KEY=${SSHD_DIR}/host_rsa_key
DSA_KEY=${SSHD_DIR}/host_dsa_key

[ -d $SSHD_DIR ] || mkdir -p $SSHD_DIR

[ -f $RSA_KEY ] || ssh-keygen -f $RSA_KEY -N '' -t rsa
[ -f $DSA_KEY ] || ssh-keygen -f $DSA_KEY -N '' -t dsa

if [ ! -f $SSHD_DIR/sshd_config ]; then

cat > $SSHD_DIR/sshd_config <<EOF
Port 2222
HostKey ${RSA_KEY}
HostKey ${DSA_KEY}
AuthorizedKeysFile $HOME/.ssh/authorized_keys
ChallengeResponseAuthentication no
UsePAM yes
Subsystem  sftp  /usr/lib/ssh/sftp-server
PidFile ${SSHD_DIR}/sshd.pid
EOF

fi
